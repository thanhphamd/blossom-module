package com.harveynash.vn.pizza.service;

import com.harveynash.vn.pizza.domain.Customer;

public interface CustomerService {
   Customer lookupCustomer(String phoneNumber) throws CustomerNotFoundException;
}
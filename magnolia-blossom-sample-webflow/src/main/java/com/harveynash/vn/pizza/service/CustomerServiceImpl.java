package com.harveynash.vn.pizza.service;

import com.harveynash.vn.pizza.domain.Customer;

public class CustomerServiceImpl implements CustomerService {
	public CustomerServiceImpl() {
	}

	public Customer lookupCustomer(String phoneNumber) throws CustomerNotFoundException {
		if ("123456".equals(phoneNumber)) {
			Customer customer = new Customer();
			customer.setName("Thanh Pham");
			customer.setAddress("Tang 6 HITC Building");
			customer.setCity("Hanoi");
			customer.setState("Caugiay");
			customer.setZipCode("123");
			customer.setPhoneNumber(phoneNumber);
			return customer;
		} else {
			throw new CustomerNotFoundException();
		}
	}
}

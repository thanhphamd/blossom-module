package com.harveynash.vn.pizza.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.harveynash.vn.pizza.domain.Customer;

@Component
public class PizzaFlowActions {
  
   public Customer lookupCustomer(String phoneNumber) 
         throws CustomerNotFoundException {     
      Customer customer = customerService.lookupCustomer(phoneNumber);
      if(customer != null) {        
        return customer;
      } else {
        throw new CustomerNotFoundException();
      }
   }
   
   public boolean checkDeliveryArea(String zipCode) {
	     return "123".equals(zipCode);
	   }

   @Autowired
   CustomerService customerService ;
}

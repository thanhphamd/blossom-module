package info.magnolia.blossom.sample.webflow;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.webflow.executor.FlowExecutor;

import info.magnolia.module.blossom.annotation.Template;

@Controller
@Template(title = "Customer Registration Flow", id = "myProjectModule:components/customerRegisterFlow")
public class CustomerRegisterFlowComponent extends AbstractSingleFlowController {
	public CustomerRegisterFlowComponent() {
		super("customer-flow");
	}

	@Override
	@Autowired
	public void setFlowExecutor(FlowExecutor flowExecutor) {
		super.setFlowExecutor(flowExecutor);
	}

	@Override
	@RequestMapping("/customerRegister-flow")
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		return super.handleRequest(request, response);
	}
}

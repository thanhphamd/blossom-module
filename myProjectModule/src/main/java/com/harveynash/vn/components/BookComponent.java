package com.harveynash.vn.components;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.harveynash.vn.model.Book;
import com.harveynash.vn.service.BookService;

import info.magnolia.module.blossom.annotation.Template;

@Controller
@Template(title = "Book", id = "myProjectModule:components/book")
@Promo
public class BookComponent {
	
	@Autowired
	private BookService bookSerivce;
	
	/* @RequestMapping("/book")
	 public String render() {
		 	
	        return "components/book.jsp";
	    }*/
	 
	 @RequestMapping("/book")
	 public ModelAndView render(){
		 
		 ModelAndView mav = new ModelAndView("components/book.jsp");
		 List<Book> listBooks = bookSerivce.getAllBooks();
		 mav.addObject("books", listBooks);
		 return mav;
	 }
	 
	 

}

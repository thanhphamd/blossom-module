package com.harveynash.vn.components;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;
import info.magnolia.dam.app.ui.config.DamConfig;

@Controller
@Template(title = "Text" , id= "myProjectModule:components/text")
@Promo
public class TextComponent {
	
	@RequestMapping("/text")
	public String render()
	{
		 return "components/text.jsp";
		
	}
	
	@TabFactory("Content")
	public void contentTab(UiConfig conf, DamConfig dam, TabBuilder tab)
	{
		tab.fields(
				conf.fields.text("heading").label("Heading"),
				conf.fields.checkbox("check").label("Check Box"),
				dam.fields.assetLink("photo").label("Photo"),
				conf.fields.richText("body").label("Text Body")
				);
	}
	

}

package com.harveynash.vn.components;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

@Controller
@Template(title = "Youtube", id = "myProjectModule:components/youtube")
@TemplateDescription("Embeds a youtube page")
public class YoutubeComponent {
	
	@RequestMapping("/youtube")
    public String render(Node node, ModelMap model) throws RepositoryException {
        model.put("videoId", node.getProperty("videoId").getString());
        return "components/youtube.jsp";
    }
	
	 @TabFactory("Content")
	    public void contentTab(UiConfig cfg, TabBuilder tab) {
	        tab.fields(
	                cfg.fields.text("videoId").label("Video id")
	        );
	    }
	
}

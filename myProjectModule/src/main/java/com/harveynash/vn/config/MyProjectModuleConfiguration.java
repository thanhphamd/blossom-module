package com.harveynash.vn.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration
@ImportResource("classpath:/info/magnolia/blossom/sample/webflow/webflow-config.xml")
public class MyProjectModuleConfiguration {

}

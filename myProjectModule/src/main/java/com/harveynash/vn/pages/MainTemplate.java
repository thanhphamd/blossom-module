package com.harveynash.vn.pages;

import javax.jcr.Node;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.harveynash.vn.components.BookComponent;
import com.harveynash.vn.components.ContactFormComponent;
import com.harveynash.vn.components.Promo;
import com.harveynash.vn.components.TextComponent;
import com.harveynash.vn.components.YoutubeComponent;

import info.magnolia.blossom.sample.webflow.BookingFlowComponent;
import info.magnolia.blossom.sample.webflow.CustomerRegisterFlowComponent;
import info.magnolia.module.blossom.annotation.Area;
import info.magnolia.module.blossom.annotation.Available;
import info.magnolia.module.blossom.annotation.AvailableComponentClasses;
import info.magnolia.module.blossom.annotation.Inherits;
import info.magnolia.module.blossom.annotation.Template;

@Controller
@Template(title ="Main Template", id = "myProjectModule:pages/main")
public class MainTemplate {
	
	@RequestMapping("/main")
	public String render()
		{
			return "pages/main.ftl";
		
		}
	
	@Available
	public boolean isAvailable(Node websiteNode) {
	    // Replace this with logic for your specific use case
	    return true;
	}
	
	
	@Area(value = "promos", title = "Promos", maxComponents=2)
	@Controller
	@AvailableComponentClasses({Promo.class})
	public static class PromosArea{
		
		@RequestMapping("mainTemplate/promos")
		public String render(){
			return "pages/areas/promos.ftl";
		}
	}
	
	
	@Area(value = "header", title ="Header")
	@Controller
	@Inherits
	@AvailableComponentClasses({YoutubeComponent.class, TextComponent.class, ContactFormComponent.class, BookingFlowComponent.class, CustomerRegisterFlowComponent.class})
	public static class HeaderArea{
		
		@RequestMapping("mainTemplate/header")
		public String render(){
			return "pages/areas/header.ftl";
		}
		
	}
	
	@Area(value = "footer", title = "Footer")
	@Controller
	@Inherits
	public static class FooterArea{
		
		@RequestMapping("mainTemplate/footer")
		public String render()
		{
			return "pages/areas/footer.ftl";
		}
	}

}

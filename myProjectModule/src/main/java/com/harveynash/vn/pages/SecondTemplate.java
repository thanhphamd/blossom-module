package com.harveynash.vn.pages;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import info.magnolia.module.blossom.annotation.Template;

@Controller
@Template(title ="Second Template", id = "myProjectModule:pages/second")
public class SecondTemplate extends MainTemplate {
	
	@RequestMapping("/second")
	public String render()
	{
		return "pages/second.ftl";
	}

}

package com.harveynash.vn.pages;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import info.magnolia.module.blossom.annotation.Template;

@Controller
@Template(id = "myProjectModule:pages/test", title = "Test Template")
public class TestTemplate {

	@RequestMapping("/test")
	public String render()
	{
		return "pages/test";
		
	}
}

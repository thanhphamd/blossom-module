package com.harveynash.vn.service;

import java.util.List;

import com.harveynash.vn.model.Book;

public interface BookService {
	public List<Book> getAllBooks();

}

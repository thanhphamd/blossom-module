package com.harveynash.vn.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.harveynash.vn.model.Book;

@Service
public class BookServiceImpl implements BookService {

	Book bookA = new Book("Rung Na Uy", "Haruki Murakami");
	Book bookB = new Book("Nhung nguoi thich dua", "Aziz Nesin");

	@Override
	public List<Book> getAllBooks() {
		List<Book> listBooks = new ArrayList<Book>();
		listBooks.add(bookA);
		listBooks.add(bookB);
		return listBooks;
	}

}

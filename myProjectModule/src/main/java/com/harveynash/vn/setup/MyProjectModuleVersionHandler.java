package com.harveynash.vn.setup;

import info.magnolia.module.DefaultModuleVersionHandler;
import info.magnolia.module.InstallContext;
import info.magnolia.module.delta.AbstractTask;
import info.magnolia.module.delta.Task;
import info.magnolia.module.delta.TaskExecutionException;
import info.magnolia.module.files.BasicFileExtractor;
import info.magnolia.module.files.ModuleFileExtractorTransformer;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
/**
 * This class is optional and lets you manage the versions of your module,
 * by registering "deltas" to maintain the module's configuration, or other type of content.
 * If you don't need this, simply remove the reference to this class in the module descriptor xml.
 *
 * @see info.magnolia.module.DefaultModuleVersionHandler
 * @see info.magnolia.module.ModuleVersionHandler
 * @see info.magnolia.module.delta.Task
 */
public class MyProjectModuleVersionHandler extends DefaultModuleVersionHandler {
	protected List<Task> getStartupTasks(final InstallContext ctx) {

        // extracts files from mgnl-files on every startup - useful for development
        Task extractTask = new AbstractTask("Files extraction", "Extracts module files but don't perform any md5 checks.") {
            public void execute(InstallContext installContext) throws TaskExecutionException {
                final String moduleName = ctx.getCurrentModuleDefinition().getName();
                try {
                    new BasicFileExtractor().extractFiles(new ModuleFileExtractorTransformer(moduleName));
                } catch (IOException e) {
                    throw new TaskExecutionException("Could not extract files for module " + ctx.getCurrentModuleDefinition() + ": " + e.getMessage(), e);
                }
            }
        };
        return Collections.singletonList(extractTask);
    }
}
<%@ include file="../includes/taglibs.jsp"%>

<h1>${content.heading}</h1>

<c:if test="${not empty content.photo}">
    <img style="float: right;margin: 10px;" src="${damfn.getAssetLink(content.photo)}"/>
</c:if>

<p>${cmsfn:decode(content).body}</p>

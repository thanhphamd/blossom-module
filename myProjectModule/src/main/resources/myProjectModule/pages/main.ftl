<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="sv" lang="sv">
<head>
<title>${content.title!}</title>
<style type="text/css">

	body { font-family: "Lucida Sans Unicode","Lucida Grande",Verdana,Arial,Helvetica,sans-serif; font-size: 13px; background-color: #DDDDDD; }
    a {color: #4040ff}
    a:visited {color: #4040ff;}
    #container {margin-left:50px; width: 90%}
    #logo {font-family:Georgia,'Times New Roman',Times,serif;font-size:46px;padding:50px 0px 8px 10px;background-color:#ffffff;}
    #content {background-color:white;padding:15px;margin-bottom:20px;-moz-border-radius-bottomleft:5px;-moz-border-radius-bottomright:5px;}
	#header { float:left; width: 70%; background-color: #ffffff;}
	#promos { float:left; width: 20%; background-color: #ffffff;margin-left:20px;}

</style>
    [@cms.page /]
</head>
<body>
    <div id="container">

        <div id="logo">This is the Main Template</div>


        <div id="content">

            [@cms.area name="header"/]

            [@cms.area name="promos"/]

			[@cms.area name="footer"/]

        </div>
    </div>
</body>
</html>
